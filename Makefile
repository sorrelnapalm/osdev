as=as
asflags=

cc=gcc
cflags=-ffreestanding -Wall -Werror -Wextra

c_sources=$(wildcard kernel/*.c drivers/*.c util/*.c)
c_headers=$(wildcard kernel/*.h drivers/*.h util/*.h)
objs=${c_sources:.c=.o}

bochs=bochs
qemu=qemu-system-x86_64

kernel.img: boot.bin kernel.bin
	cat $^ > $@

kernel.bin: kernel/kernel_entry.o $(objs)
	ld -o $@ -Ttext 0x1000 $^ --oformat binary

%.o: %.c $(c_headers)
	$(cc) $(cflags) -c $< -o $@

%.o: %.s
	$(as) $(asflags) -o $@ $<

boot.bin: boot/boot.o
	ld -Ttext 0x7c00 --oformat=binary -o $@ $<

all: kernel.img

run: kernel.img
	$(qemu) -drive format=raw,file=$<

debug: kernel.img
	objdump -b binary -D -m i8086 --stop-address=0x200 $<
	objdump -b binary -D -m i386 --stop-address=0x200 $<
	objdump -b binary -D -m i386:x86-64 --stop-address=0x200 $<
	objdump -b binary -D -m i386:x86-64 --start-address=0x200 $<
	$(qemu) -s -S -m 512 -drive format=raw,file=$<

.PHONY: clean debug run all
clean:
	rm -f boot/*.o kernel/*.o drivers/*.o util/*.o boot.bin kernel.bin kernel.img
