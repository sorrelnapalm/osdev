.code16
.text
	.globl _start

.set KERNEL_OFFSET, 0x1000

_start:
	# loaded at 0x7c00

	movb %dl, (BOOT_DRIVE)

	# set up stack
	movw $0x9000, %bp
	movw %bp, %sp

	pushw $REAL_MODE_MSG
	call print_str

	# clear local vars
	movw %bp, %sp

	# load kernel
	pushw $10 # num sectors to read
	pushw $2 # first sector to read, 1-indexed
	pushw $0 # head
	pushw $0 # cylinder
	movb (BOOT_DRIVE), %dl
	xorb %dh, %dh
	pushw %dx # boot drive

	xorw %bx, %bx
	movw %bx, %es
	movw $KERNEL_OFFSET, %bx

	call disk_load
	addw $10, %sp
	popw %dx

	# switch to protected mode
	cli

	lgdt gdt_descriptor
	movl %cr0, %eax
	orl $0x01, %eax
	movl %eax, %cr0

	# flush pipeline using far jump (0x08 is code seg offset)
	jmp $0x08, $init_pm

	jmp .

gdt:

gdt_null:
	.skip 8

gdt_code:
	/* base=0x0, limit=0xfffff
	 * 1st flags: present=1, privilege=00, descriptor type 1 -> 1001b
	 * type flags: code=1, conforming=0, readable=1, accessed=0 -> 1010b
	 * 2nd flags: granularity=1, 32-bit default=1, 64-bit seg=0, avl=0 -> 1100b
	*/
	.word 0xffff # limit, bits 0-15
	.word 0x0 # base adrs, bits 0-15
	.byte 0x0 # base, bits 16-23
	.byte 0x9a # 1st and type flags
	.byte 0xcf # 2nd flags and limit bits 16-19
	.byte 0x0 # base bits, 24-31

gdt_data:
	/* base=0x0, limit=0xfffff
	 * 1st flags: present=1, privilege=00, descriptor type 1 -> 1001b
	 * type flags: code=0, expand down=0, writable=1, accessed=0 -> 0010b
	 * 2nd flags: granularity=1, 32-bit default=1, 64-bit seg=0, avl=0 -> 1100b
	*/
	.word 0xffff # limit, bits 0-15
	.word 0x0 # base adrs, bits 0-15
	.byte 0x0 # base, bits 16-23
	.byte 0x92 # 1st and type flags
	.byte 0xcf # 2nd flags and limit bits 16-19
	.byte 0x0 # base bits, 24-31
gdt_end:

gdt_descriptor:
	.word gdt_end - gdt - 1
	.long gdt

# gdt segment descriptor offsets
.set CODE_SEG, gdt_code - gdt
.set DATA_SEG, gdt_data - gdt

# 64-bit gdt
# segmentation is disabled in 64-bit mode; code segs span all of virtual mem
# base address treated as 0; segment limit ignored; readable (R) and accessed
# (A) ignored; granularity (G) ignored
gdt64:
gdt64_null:
	.skip 8
gdt64_code:
	.word 0xffff # limit, bits 0-15
	.word 0x00  # base, bits 0-15
	.byte 0x00 # base, bits 16-23
	.byte 0x9a # 1st flags: (bit 15) P=1, DPL=00, 1, 1, C=0, R=1, A=0 (bit 8)
	.byte 0xaf# 2nd flags and seg limit 16-19: (bit 23) G=1, D=0, L=1, AVL,
	      # lim: f (bit 16)
	.byte 0x00 # base bits 24-31

gdt64_data:
	.word 0xffff # limit, bits 0-15
	.word 0x00  # base, bits 0-15
	.byte 0x00 # base, bits 16-23
	.byte 0x92 # 1st flags: (bit 15) P=1, DPL=00, 1, 0, E=0, W=1, A=0 (bit 8)
	.byte 0x8f# 2nd flags and seg limit 16-19: (bit 23) G=1, D/B=0, 0, AVL,
	      # lim: f (bit 16)
	.byte 0x00 # base bits 24-31

gdt64_end:

.set CODE_SEG_64, gdt64_code - gdt64
.set DATA_SEG_64, gdt64_data - gdt64

gdt64_descriptor:
	.word gdt64_end - gdt64 - 1
	.long gdt64

/* constants */

BOOT_DRIVE:
	.byte 0

DISK_ERROR_MSG:
	.asciz "Disk read error!\n"

REAL_MODE_MSG:
	.asciz "In 16-bit real mode\n"

/* functions */

/* disk_load: load sectors from disk %dl
 *	arguments:
 *		1. drive number
 *		2. cylinder
 *		3. head
 *		4. first sector to read (1-indexed)
 *		5. number of sectors to read
 *	returns: %ax: sectors read
 *	clobbers: %ax
*/
disk_load:
	# call stack
	pushw %bp
	movw %sp, %bp
	pushw %bx
	pushw %cx
	pushw %dx

	# set up registers
	movw 4(%bp), %dx # drive num
	movw 6(%bp), %ax
	movb %al, %ch # cylinder
	movw 8(%bp), %ax
	movb %al, %dh # head
	movw 10(%bp), %ax
	movb %al, %cl # first sector to read
	movw 12(%bp), %ax # num sectors to read

	# contents loaded to %es:(%bx)
	movb $0x02, %ah # BIOS read sector function

	int $0x13

	jnc disk_no_error
	pushw $DISK_ERROR_MSG
	call print_str
	jmp .

disk_no_error:
	# return num sectors read
	popw %ax

	# tear down stack frame
	popw %dx
	popw %cx
	popw %bx
	movw %bp, %sp
	popw %bp
	ret

/* print_str: print a string to the screen using BIOS routine
 * 	arguments: 1. pointer to beginning of zero-terminated string
 * 	returns: nothing
 * 	clobbers: nothing
 *	caveats: naively prints until it encounters a 0 byte
*/
print_str:
	# call stack
	pushw %bp
	movw %sp, %bp

	# callee-saved
	pushw %ax
	pushw %bx

	# body
	movw 4(%bp), %bx # arg 1

print_loop:
	movb $0x0e, %ah
	movb (%bx), %al
	test %al, %al
	jz print_end
	int $0x10
	addw $1, %bx
	jmp print_loop

print_end:
	# restore callee-saved
	popw %bx
	popw %ax
	# tear down stack frame
	movw %bp, %sp
	popw %bp
	ret

.code32

init_pm:
	movw $DATA_SEG, %ax
	movw %ax, %ds
	movw %ax, %ss
	movw %ax, %es
	movw %ax, %fs
	movw %ax, %gs

	# new stack position
	movl $0x90000, %ebp
	movl %ebp, %esp

	/* switch to long mode */

	/* disable paging bit */
	movl %cr0, %eax
	andl $0x7fffffff, %eax
	movl %eax, %cr0

	/* enable PAE; required in long mode */
	movl %cr4, %eax
	/* ... 0010 0000 */
	orl $0x20, %eax
	movl %eax, %cr4

	/* set up paging for long mode
	 * 64-bit virtual address:
	 * 63       48 47      39 38     30 29    21 20    12 11        0
	 *  | signext | pml4 off | pdp off | pd off | pt off | page off |
	 * %cr3 contains the address of the pml4 table
	 *
	 * CR4.PAE=1, PDPE.PS=0, PDE.PS=0 -> 4 KB pages, 64-bit VAs, 52-bit PAs
	 * both PS bits are bit 7
	 * set up tables so PML4T[0] -> PDPT, PDPT[0] -> PDT, PDT[0] -> PT,
	 * PT -> 0x0 - 0x200000 (2MB)
	*/
	/* clear tables */
	movl  $0x10000, %edi
	movl %edi, %cr3
	xorl %eax, %eax
	movl $4096, %ecx
	rep stosl
	movl %cr3, %edi

	/* set up pml4 at 0x10000, pdpt at 0x11000, etc. */
	movl $0x11003, (%edi) # pml4t
	addl $0x1000, %edi
	movl $0x12003, (%edi) # pdpt
	addl $0x1000, %edi
	movl $0x13003, (%edi) # pdt
	addl $0x1000, %edi

	/* identity map first 2 MB in PT */
	movl $0x3, %ebx
	movl $512, %ecx
.setpte:
	movl %ebx, (%edi)
	addl $0x1000, %ebx
	addl $8, %edi
	loop .setpte

	/* enter compatibility mode */
	movl $0xc0000080, %ecx
	rdmsr
	orl $0x100, %eax
	wrmsr

	/* enable paging */
	movl %cr0, %eax
	orl $0x80000000, %eax
	movl %eax, %cr0

	/* 64-bit gdt */
	lgdt gdt64_descriptor

	# flush pipeline using far jump (0x08 is code seg offset)
	jmp $0x08, $init_lm

.code64
init_lm:
	movw $DATA_SEG_64, %ax
	movw %ax, %ds
	movw %ax, %es
	movw %ax, %fs
	movw %ax, %gs
	movw %ax, %ss

	jmp KERNEL_OFFSET

_skip:
	.set SKIP_SIZE, 510 - (_skip - _start)
	.skip SKIP_SIZE

	# so BIOS knows this is a boot sector
	.word 0xaa55
