#include "idt.h"

#include "screen.h"
#include "../util/string.h"

extern void _isr(void);

void init_idt(void) {
	__asm__("cli");

	struct interrupt_descriptor_64* const idt_start =
		(struct interrupt_descriptor_64* const)IDT_ADDRESS;
	for (int i = 0; i < 256; ++i) {
		uint16_t selector = 1 << 3; // selector index; use code segment
		selector &= 0xfffffff8; // TI=0 (bit 2) -> use gdt, RPL=00 (bits 0-1)

		uint64_t offset = (uint64_t)(_isr); // address of the entry point of ISR
		uint16_t offset_0 = (uint16_t)(offset & 0xffff);
		uint16_t offset_1 = (uint16_t)((offset >> 16) & 0xffff);
		uint32_t offset_2 = (uint32_t)(offset >> 32);
		uint8_t type = 0;

		type = type | (1 << 7); // present=1
		type = type | 0xe; // gate type 0xe = interrupt gate

		idt_start[i].offset_0 = offset_0;
		idt_start[i].offset_1 = offset_1;
		idt_start[i].offset_2 = offset_2;
		idt_start[i].selector = selector;
		idt_start[i].reserved_0 = (uint8_t)0;
		idt_start[i].reserved_1 = (uint32_t)0;
		idt_start[i].type = type;
	}

	//struct idt_descriptor_64* const idt_desc_table =
	//    (struct idt_descriptor_64* const)(IDT_ADDRESS + 256 * 16);
	//idt_desc_table[0].len = 255;
	//idt_desc_table[0].adrs = IDT_ADDRESS;
	uint16_t* const idt_desc_table = (uint16_t* const)(IDT_ADDRESS + 256 * 16);
	idt_desc_table[0] = 255;
	idt_desc_table[1] = 0x9000;
	idt_desc_table[2] = 0;
	idt_desc_table[3] = 0;
	idt_desc_table[4] = 0;

	__asm__("lidt 0xa000");
	__asm__("sti");
}

void interrupt_handler(void) {
	string z = make_string("int handler\n");
	print(z);
}
