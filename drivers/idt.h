#ifndef DRIVERS_IDT_H
#define DRIVERS_IDT_H

#include "../util/types.h"

#define IDT_ADDRESS 0x9000

struct interrupt_descriptor_64 {
	uint16_t offset_0;
	uint16_t selector;
	uint8_t reserved_0; // zero
	uint8_t type; // gate type 0-3, 0, DPL 5-6, present 7
	uint16_t offset_1;
	uint32_t offset_2;
	uint32_t reserved_1; // zero
};

void init_idt(void);

#endif // DRIVERS_IDT_H
