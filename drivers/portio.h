#ifndef DRIVERS_PORTIO_H
#define DRIVERS_PORTIO_H

#include "../util/types.h"

uint8_t port_read_byte(uint16_t const port);
uint16_t port_read_word(uint16_t const port);

void port_write_byte(uint16_t const port, uint8_t const data);
void port_write_word(uint16_t const port, uint16_t const data);

#endif // DRIVERS_PORTIO_H
