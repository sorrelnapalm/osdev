#include "screen.h"

#include "portio.h"
#include "../util/string.h"

/* functions intended to be used in external sources */

void init_screen(void) {
	clear_screen();
	_set_cursor(0, 0);
}

void clear_screen() {
	uint16_t* const vidmem = (uint16_t* const) VIDEO_MEM;
	for (uint32_t i = 0; i < MAX_ROWS * MAX_COLS; ++i) {
		vidmem[i] = ((uint16_t)TERM_COLOR) << 8;
	}
}

void print(string const str) {
	uint32_t c = str.start;
	while (c < str.start + str.len && c < str.end) {
		_print_char(str.str[c]);
		++c;
	}
	_print_char('\n');
}

void print_char(uint8_t const c) {
	_print_char(c);
}

/* internal functions */

void _clear_row(uint32_t const row) {
	if (row < MAX_ROWS) {
		uint16_t* const vidmem = (uint16_t* const) VIDEO_MEM;
		for (uint32_t i = 0; i < MAX_COLS; ++i) {
			// keep attribute the same
			vidmem[row * MAX_COLS + i] = ((uint16_t)TERM_COLOR << 8);
		}
	}
}

void _copy_row(uint32_t const from, uint32_t const to) {
	if (to >= MAX_ROWS || from >= MAX_ROWS) {
		return;
	}

	uint32_t const offset_from = from * MAX_COLS;
	uint32_t const offset_to = to * MAX_COLS;
	uint16_t* const vidmem = (uint16_t* const) VIDEO_MEM;
	for (uint32_t i = 0; i < MAX_COLS; ++i) {
		vidmem[offset_to+i] = vidmem[offset_from+i];
	}
}

uint32_t _get_cursor(void) {
	uint32_t offset = 0; // char offset
	port_write_byte(REG_SCREEN_CTRL, 0xe); // cursor offset hi byte
	offset += port_read_byte(REG_SCREEN_DATA) << 8;
	port_write_byte(REG_SCREEN_CTRL, 0xf); // lo byte
	offset += port_read_byte(REG_SCREEN_DATA);

	return offset;
}

uint32_t _get_cursor_x(void) {
	uint32_t const offset = _get_cursor();
	return offset % MAX_COLS;
}

uint32_t _get_cursor_y(void) {
	uint32_t const offset = _get_cursor();
	return offset / MAX_COLS;
}

void _set_cursor_offset(uint32_t const offset) {
	if (offset >= MAX_ROWS * MAX_COLS) {
		return;
	}

	uint8_t upper = (char)((offset >> 8) & 0xff);
	uint8_t lower = (char)(offset & 0xff);

	port_write_byte(REG_SCREEN_CTRL, 0xe);
	port_write_byte(REG_SCREEN_DATA, upper);
	port_write_byte(REG_SCREEN_CTRL, 0xf);
	port_write_byte(REG_SCREEN_DATA, lower);
}

inline void _set_cursor(uint32_t const x, uint32_t const y) {
	_set_cursor_offset(x + y * MAX_COLS);
}

// puts a char at an x, y location. does not handle the cursor, newlines, tabs.
void _print_char_at(uint8_t const c, uint32_t const x, uint32_t const y) {
	if (c == '\n' || c == '\t') {
		return;
	} else {
		uint32_t idx = x + y * MAX_COLS;
		if (idx < MAX_ROWS * MAX_COLS) {
			uint16_t* const vidmem = (uint16_t* const) VIDEO_MEM;
			// char is lower byte, attrib is higher byte
			vidmem[idx] = ((uint16_t)TERM_COLOR << 8) + c;
		}
	}
}

/* prints a char at the cursor location, then increments the cursor.
 * does handle newlines, wrapping, scrolling
*/
void _print_char(uint8_t const c) {
	uint32_t const cur_x = _get_cursor_x();
	uint32_t const cur_y = _get_cursor_y();

	if (c == '\n') {
		if (cur_y < MAX_ROWS - 1) {
			_set_cursor(0, cur_y + 1);
		} else {
			_scroll();
		}
	} else if (c == '\t') {
		uint32_t const tabs_to_print = ((cur_x / TABSIZE) + 1) * TABSIZE - cur_x;
		for (uint32_t i = 0; i < tabs_to_print; ++i) {
			_print_char(' ');
		}
	} else {
		// print at current cursor location
		_print_char_at(c, cur_x, cur_y);

		// increment cursor
		if (cur_x < MAX_COLS - 1) {
				_set_cursor(cur_x + 1, cur_y);
		} else {
			if (cur_y < MAX_ROWS - 1) {
				// wrap to next line, don't need to scroll
				_set_cursor(0, cur_y + 1);
			} else {
				_scroll();
			}
		}
	}
}

/* print zero-terminated string */
void _print_char_str(char const * const str, uint32_t const len) {
	uint32_t c = 0;
	while (str[c] && c < len) {
		_print_char(str[c]);
		++c;
	}
	_print_char('\n');
}

void _scroll(void) {
	// copy each line to the one above
	for (uint32_t i = 1; i < MAX_ROWS; ++i) {
		_clear_row(i - 1);
		_copy_row(i, i - 1);
	}

	// start printing from the last line
	_clear_row(MAX_ROWS - 1);
	_set_cursor(0, MAX_ROWS - 1);
}
