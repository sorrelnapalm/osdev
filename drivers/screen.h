#ifndef DRIVERS_SCREEN_H
#define DRIVERS_SCREEN_H

#include "../util/string.h"
#include "../util/types.h"

#define MAX_ROWS 25
#define MAX_COLS 80

#define REG_SCREEN_CTRL 0x3d4
#define REG_SCREEN_DATA 0x3d5

#define VIDEO_MEM 0xb8000
#define TERM_COLOR 0x0f

#define TABSIZE 8

extern uint8_t term_color;

void init_screen(void);
void clear_screen(void);
void print(string const str);
void print_char(uint8_t const c);

void _clear_row(uint32_t const row);
void _copy_row(uint32_t const from, uint32_t const to);

uint32_t _get_cursor_x(void);
uint32_t _get_cursor_y(void);
void _set_cursor_offset(uint32_t const offset);
void _set_cursor(uint32_t const x, uint32_t const y);

void _scroll(void);

void _print_char_at(uint8_t const c, uint32_t const x, uint32_t const y);
void _print_char(uint8_t const c);
void _print_char_str(char const * const str, uint32_t const len);

#endif // DRIVERS_SCREEN_H
