
#include "../drivers/screen.h"
#include "../drivers/idt.h"
#include "../util/string.h"
#include "../util/types.h"

void main(void) {
	init_screen();
	string z = make_string("hello world!\n");
	print(z);
	print(z);
	init_idt();
}
