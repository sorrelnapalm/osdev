.code64

.text
.globl _start

_start:

call main
jmp .

.globl _isr

_isr:
	cli
	pushq %rax
	pushq %rbx
	pushq %rcx
	pushq %rdx
	pushq %rbp
	pushq %rsi
	pushq %rdi
	cld
	call interrupt_handler
	popq %rdi
	popq %rsi
	popq %rbp
	popq %rdx
	popq %rcx
	popq %rbx
	popq %rax
	sti
	iretq
