#include "string.h"

string make_string(char* s) {
	uint32_t len = 0;
	while (s[len]) {
		++len;
	}
	string str = { .start=0, .end=len, .len=len, .str=s };
	return str;
}
