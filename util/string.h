#ifndef UTIL_STRING_H
#define UTIL_STRING_H

#include "types.h"

typedef struct _string {
	uint32_t const start;
	uint32_t const end;
	uint32_t const len;
	char* const str;
} string;

string make_string(char* s);

#endif // UTIL_STRING_H
