#ifndef UTIL_TYPES_H
#define UTIL_TYPES_H

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;

#endif // UTIL_TYPES_H
